
using System;

class rungekutta
{
    /*
    This members must be defined by you:
    static double f(double x,
        double y)
    */


    /*************************************************************************
    ����� �����-����� ���������� ������� ��� ������� ��������� ������� �������.

    function RungeKutt(x,x1,y:real;n:integer):real;

    �������� ������ ������ y'=F(x,y) ������� �����-����� 4 �������.

    ��������� ����� ����� ��������� (x,y),
    �������� - (x1, RungeKutt(x, x1, y, n)).

    �� �������� ����� �� ���������� ����� n �������������
    � ���������� ����� h=(x1-x)/n
    *************************************************************************/
    public static double solveoderungekutta(double x,
        double x1,
        double y,
        int n)
    {
        double result = 0;
        int i = 0;
        double h = 0;
        double y1 = 0;
        double k1 = 0;
        double k2 = 0;
        double k3 = 0;

        h = (x1-x)/n;
        y1 = y;
        i = 1;
        do
        {
            k1 = h*f(x, y);
            x = x+h/2;
            y = y1+k1/2;
            k2 = f(x, y)*h;
            y = y1+k2/2;
            k3 = f(x, y)*h;
            x = x+h/2;
            y = y1+k3;
            y = y1+(k1+2*k2+2*k3+f(x, y)*h)/6;
            y1 = y;
            i = i+1;
        }
        while( i<=n );
        result = y;
        return result;
    }
}
