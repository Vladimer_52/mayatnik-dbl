﻿namespace Mayatnik
{
    partial class Form1
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.bunifuElipse1 = new Bunifu.Framework.UI.BunifuElipse(this.components);
            this.Head = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.Button_Close = new Bunifu.Framework.UI.BunifuImageButton();
            this.bunifuImageButton1 = new Bunifu.Framework.UI.BunifuImageButton();
            this.Button_minimaze = new Bunifu.Framework.UI.BunifuImageButton();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.bunifuDragControl1 = new Bunifu.Framework.UI.BunifuDragControl(this.components);
            this.panel1 = new System.Windows.Forms.Panel();
            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.panel3 = new System.Windows.Forms.Panel();
            this.Button_Stop = new Bunifu.Framework.UI.BunifuFlatButton();
            this.Button_Run = new Bunifu.Framework.UI.BunifuFlatButton();
            this.ris_main = new System.Windows.Forms.PictureBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.Slider_2 = new Bunifu.Framework.UI.BunifuSlider();
            this.Slider_1 = new Bunifu.Framework.UI.BunifuSlider();
            this.text_h = new System.Windows.Forms.TextBox();
            this.text_m2 = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.text_f2 = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.text_m1 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.text_f1 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.chart2 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.Head.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Button_Close)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bunifuImageButton1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Button_minimaze)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ris_main)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart2)).BeginInit();
            this.SuspendLayout();
            // 
            // bunifuElipse1
            // 
            this.bunifuElipse1.ElipseRadius = 3;
            this.bunifuElipse1.TargetControl = this;
            // 
            // Head
            // 
            this.Head.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(214)))), ((int)(((byte)(51)))));
            this.Head.Controls.Add(this.label1);
            this.Head.Controls.Add(this.Button_Close);
            this.Head.Controls.Add(this.bunifuImageButton1);
            this.Head.Controls.Add(this.Button_minimaze);
            this.Head.Dock = System.Windows.Forms.DockStyle.Top;
            this.Head.Location = new System.Drawing.Point(0, 0);
            this.Head.Name = "Head";
            this.Head.Size = new System.Drawing.Size(1218, 45);
            this.Head.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(201, 25);
            this.label1.TabIndex = 2;
            this.label1.Text = "Двойной маятник";
            // 
            // Button_Close
            // 
            this.Button_Close.BackColor = System.Drawing.Color.Transparent;
            this.Button_Close.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Button_Close.Image = ((System.Drawing.Image)(resources.GetObject("Button_Close.Image")));
            this.Button_Close.ImageActive = null;
            this.Button_Close.Location = new System.Drawing.Point(1171, 4);
            this.Button_Close.Name = "Button_Close";
            this.Button_Close.Size = new System.Drawing.Size(35, 35);
            this.Button_Close.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Button_Close.TabIndex = 1;
            this.Button_Close.TabStop = false;
            this.Button_Close.Zoom = 10;
            this.Button_Close.Click += new System.EventHandler(this.Button_Close_Click);
            // 
            // bunifuImageButton1
            // 
            this.bunifuImageButton1.BackColor = System.Drawing.Color.Transparent;
            this.bunifuImageButton1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuImageButton1.Image = ((System.Drawing.Image)(resources.GetObject("bunifuImageButton1.Image")));
            this.bunifuImageButton1.ImageActive = null;
            this.bunifuImageButton1.Location = new System.Drawing.Point(1089, 4);
            this.bunifuImageButton1.Name = "bunifuImageButton1";
            this.bunifuImageButton1.Size = new System.Drawing.Size(35, 35);
            this.bunifuImageButton1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.bunifuImageButton1.TabIndex = 1;
            this.bunifuImageButton1.TabStop = false;
            this.bunifuImageButton1.Zoom = 5;
            this.bunifuImageButton1.Click += new System.EventHandler(this.Button_Reboot_Click);
            // 
            // Button_minimaze
            // 
            this.Button_minimaze.BackColor = System.Drawing.Color.Transparent;
            this.Button_minimaze.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Button_minimaze.Image = ((System.Drawing.Image)(resources.GetObject("Button_minimaze.Image")));
            this.Button_minimaze.ImageActive = null;
            this.Button_minimaze.Location = new System.Drawing.Point(1130, 4);
            this.Button_minimaze.Name = "Button_minimaze";
            this.Button_minimaze.Size = new System.Drawing.Size(35, 35);
            this.Button_minimaze.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Button_minimaze.TabIndex = 1;
            this.Button_minimaze.TabStop = false;
            this.Button_minimaze.Zoom = 10;
            this.Button_minimaze.Click += new System.EventHandler(this.Button_minimaze_Click);
            // 
            // bunifuDragControl1
            // 
            this.bunifuDragControl1.Fixed = true;
            this.bunifuDragControl1.Horizontal = true;
            this.bunifuDragControl1.TargetControl = this.Head;
            this.bunifuDragControl1.Vertical = true;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panel1.Controls.Add(this.chart2);
            this.panel1.Controls.Add(this.chart1);
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Controls.Add(this.Button_Stop);
            this.panel1.Controls.Add(this.Button_Run);
            this.panel1.Controls.Add(this.ris_main);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 45);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1218, 655);
            this.panel1.TabIndex = 1;
            // 
            // chart1
            // 
            chartArea2.AxisX.LineColor = System.Drawing.Color.White;
            chartArea2.AxisX.MajorGrid.LineColor = System.Drawing.Color.White;
            chartArea2.AxisX.MajorTickMark.LineColor = System.Drawing.Color.White;
            chartArea2.AxisX.Maximum = 300D;
            chartArea2.AxisX.Minimum = -300D;
            chartArea2.AxisX.MinorGrid.LineColor = System.Drawing.Color.White;
            chartArea2.AxisX.MinorTickMark.LineColor = System.Drawing.Color.White;
            chartArea2.AxisX.TitleForeColor = System.Drawing.Color.White;
            chartArea2.AxisY.MajorGrid.LineColor = System.Drawing.Color.White;
            chartArea2.AxisY.Maximum = 300D;
            chartArea2.AxisY.Minimum = -300D;
            chartArea2.BackColor = System.Drawing.Color.Black;
            chartArea2.BorderColor = System.Drawing.Color.White;
            chartArea2.Name = "ChartArea1";
            chartArea2.ShadowColor = System.Drawing.Color.White;
            this.chart1.ChartAreas.Add(chartArea2);
            this.chart1.Location = new System.Drawing.Point(560, 43);
            this.chart1.Name = "chart1";
            this.chart1.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.Fire;
            series2.BorderDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Dot;
            series2.BorderWidth = 3;
            series2.ChartArea = "ChartArea1";
            series2.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            series2.Name = "Series1";
            this.chart1.Series.Add(series2);
            this.chart1.Size = new System.Drawing.Size(303, 259);
            this.chart1.TabIndex = 4;
            this.chart1.Text = "chart1";
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(214)))), ((int)(((byte)(51)))));
            this.panel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel3.Location = new System.Drawing.Point(0, 645);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1218, 10);
            this.panel3.TabIndex = 3;
            // 
            // Button_Stop
            // 
            this.Button_Stop.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(14)))), ((int)(((byte)(68)))));
            this.Button_Stop.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(214)))), ((int)(((byte)(51)))));
            this.Button_Stop.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Button_Stop.BorderRadius = 2;
            this.Button_Stop.ButtonText = "остановить";
            this.Button_Stop.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Button_Stop.DisabledColor = System.Drawing.Color.Gray;
            this.Button_Stop.Enabled = false;
            this.Button_Stop.Iconcolor = System.Drawing.Color.Transparent;
            this.Button_Stop.Iconimage = ((System.Drawing.Image)(resources.GetObject("Button_Stop.Iconimage")));
            this.Button_Stop.Iconimage_right = null;
            this.Button_Stop.Iconimage_right_Selected = null;
            this.Button_Stop.Iconimage_Selected = null;
            this.Button_Stop.IconMarginLeft = 0;
            this.Button_Stop.IconMarginRight = 0;
            this.Button_Stop.IconRightVisible = true;
            this.Button_Stop.IconRightZoom = 0D;
            this.Button_Stop.IconVisible = true;
            this.Button_Stop.IconZoom = 60D;
            this.Button_Stop.IsTab = false;
            this.Button_Stop.Location = new System.Drawing.Point(1025, 437);
            this.Button_Stop.Name = "Button_Stop";
            this.Button_Stop.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(214)))), ((int)(((byte)(51)))));
            this.Button_Stop.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(241)))), ((int)(((byte)(187)))));
            this.Button_Stop.OnHoverTextColor = System.Drawing.Color.Black;
            this.Button_Stop.selected = false;
            this.Button_Stop.Size = new System.Drawing.Size(160, 48);
            this.Button_Stop.TabIndex = 2;
            this.Button_Stop.Text = "остановить";
            this.Button_Stop.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Button_Stop.Textcolor = System.Drawing.Color.Black;
            this.Button_Stop.TextFont = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Button_Stop.Click += new System.EventHandler(this.Button_Stop_Click);
            // 
            // Button_Run
            // 
            this.Button_Run.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(14)))), ((int)(((byte)(68)))));
            this.Button_Run.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(214)))), ((int)(((byte)(51)))));
            this.Button_Run.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Button_Run.BorderRadius = 2;
            this.Button_Run.ButtonText = "запустить";
            this.Button_Run.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Button_Run.DisabledColor = System.Drawing.Color.Gray;
            this.Button_Run.Iconcolor = System.Drawing.Color.Transparent;
            this.Button_Run.Iconimage = ((System.Drawing.Image)(resources.GetObject("Button_Run.Iconimage")));
            this.Button_Run.Iconimage_right = null;
            this.Button_Run.Iconimage_right_Selected = null;
            this.Button_Run.Iconimage_Selected = null;
            this.Button_Run.IconMarginLeft = 0;
            this.Button_Run.IconMarginRight = 0;
            this.Button_Run.IconRightVisible = true;
            this.Button_Run.IconRightZoom = 0D;
            this.Button_Run.IconVisible = true;
            this.Button_Run.IconZoom = 60D;
            this.Button_Run.IsTab = false;
            this.Button_Run.Location = new System.Drawing.Point(1025, 383);
            this.Button_Run.Name = "Button_Run";
            this.Button_Run.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(214)))), ((int)(((byte)(51)))));
            this.Button_Run.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(241)))), ((int)(((byte)(187)))));
            this.Button_Run.OnHoverTextColor = System.Drawing.Color.Black;
            this.Button_Run.selected = false;
            this.Button_Run.Size = new System.Drawing.Size(160, 48);
            this.Button_Run.TabIndex = 2;
            this.Button_Run.Text = "запустить";
            this.Button_Run.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Button_Run.Textcolor = System.Drawing.Color.Black;
            this.Button_Run.TextFont = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Button_Run.Click += new System.EventHandler(this.Button_Run_Click);
            // 
            // ris_main
            // 
            this.ris_main.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ris_main.Location = new System.Drawing.Point(17, 43);
            this.ris_main.Name = "ris_main";
            this.ris_main.Size = new System.Drawing.Size(500, 500);
            this.ris_main.TabIndex = 1;
            this.ris_main.TabStop = false;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(14)))), ((int)(((byte)(68)))));
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel2.Controls.Add(this.Slider_2);
            this.panel2.Controls.Add(this.Slider_1);
            this.panel2.Controls.Add(this.text_h);
            this.panel2.Controls.Add(this.text_m2);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.text_f2);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.text_m1);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.text_f1);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Location = new System.Drawing.Point(957, 43);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(228, 334);
            this.panel2.TabIndex = 0;
            // 
            // Slider_2
            // 
            this.Slider_2.BackColor = System.Drawing.Color.Transparent;
            this.Slider_2.BackgroudColor = System.Drawing.Color.White;
            this.Slider_2.BorderRadius = 0;
            this.Slider_2.IndicatorColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(214)))), ((int)(((byte)(51)))));
            this.Slider_2.Location = new System.Drawing.Point(78, 134);
            this.Slider_2.MaximumValue = 360;
            this.Slider_2.Name = "Slider_2";
            this.Slider_2.Size = new System.Drawing.Size(133, 30);
            this.Slider_2.TabIndex = 2;
            this.Slider_2.Value = 30;
            this.Slider_2.ValueChanged += new System.EventHandler(this.Slider_2_ValueChanged);
            // 
            // Slider_1
            // 
            this.Slider_1.BackColor = System.Drawing.Color.Transparent;
            this.Slider_1.BackgroudColor = System.Drawing.Color.White;
            this.Slider_1.BorderRadius = 0;
            this.Slider_1.IndicatorColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(214)))), ((int)(((byte)(51)))));
            this.Slider_1.Location = new System.Drawing.Point(78, 86);
            this.Slider_1.MaximumValue = 360;
            this.Slider_1.Name = "Slider_1";
            this.Slider_1.Size = new System.Drawing.Size(133, 30);
            this.Slider_1.TabIndex = 2;
            this.Slider_1.Value = 60;
            this.Slider_1.ValueChanged += new System.EventHandler(this.Slider_1_ValueChanged);
            // 
            // text_h
            // 
            this.text_h.Location = new System.Drawing.Point(17, 282);
            this.text_h.Name = "text_h";
            this.text_h.Size = new System.Drawing.Size(100, 20);
            this.text_h.TabIndex = 1;
            this.text_h.Text = "0,5";
            // 
            // text_m2
            // 
            this.text_m2.Location = new System.Drawing.Point(17, 230);
            this.text_m2.Name = "text_m2";
            this.text_m2.Size = new System.Drawing.Size(100, 20);
            this.text_m2.TabIndex = 1;
            this.text_m2.Text = "2,75";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label7.ForeColor = System.Drawing.Color.White;
            this.label7.Location = new System.Drawing.Point(17, 260);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(15, 16);
            this.label7.TabIndex = 0;
            this.label7.Text = "h";
            // 
            // text_f2
            // 
            this.text_f2.Location = new System.Drawing.Point(17, 134);
            this.text_f2.Name = "text_f2";
            this.text_f2.Size = new System.Drawing.Size(55, 20);
            this.text_f2.TabIndex = 1;
            this.text_f2.Text = "30";
            this.text_f2.TextChanged += new System.EventHandler(this.text_f2_TextChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(17, 208);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(24, 16);
            this.label6.TabIndex = 0;
            this.label6.Text = "m2";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(17, 112);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(26, 16);
            this.label4.TabIndex = 0;
            this.label4.Text = "ф2";
            // 
            // text_m1
            // 
            this.text_m1.Location = new System.Drawing.Point(17, 182);
            this.text_m1.Name = "text_m1";
            this.text_m1.Size = new System.Drawing.Size(100, 20);
            this.text_m1.TabIndex = 1;
            this.text_m1.Text = "1";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(17, 160);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(24, 16);
            this.label5.TabIndex = 0;
            this.label5.Text = "m1";
            // 
            // text_f1
            // 
            this.text_f1.Location = new System.Drawing.Point(17, 86);
            this.text_f1.Name = "text_f1";
            this.text_f1.Size = new System.Drawing.Size(55, 20);
            this.text_f1.TabIndex = 1;
            this.text_f1.Text = "60";
            this.text_f1.TextChanged += new System.EventHandler(this.text_f1_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(17, 64);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(26, 16);
            this.label3.TabIndex = 0;
            this.label3.Text = "ф1";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(3, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(136, 17);
            this.label2.TabIndex = 0;
            this.label2.Text = "начальные условия:";
            // 
            // timer
            // 
            this.timer.Interval = 1;
            this.timer.Tick += new System.EventHandler(this.timer_Tick);
            // 
            // chart2
            // 
            chartArea1.AxisX.LineColor = System.Drawing.Color.White;
            chartArea1.AxisX.MajorGrid.LineColor = System.Drawing.Color.White;
            chartArea1.AxisX.MajorTickMark.LineColor = System.Drawing.Color.White;
            chartArea1.AxisX.Maximum = 3D;
            chartArea1.AxisX.Minimum = -3D;
            chartArea1.AxisX.MinorGrid.LineColor = System.Drawing.Color.White;
            chartArea1.AxisX.MinorTickMark.LineColor = System.Drawing.Color.White;
            chartArea1.AxisX.TitleForeColor = System.Drawing.Color.White;
            chartArea1.AxisY.MajorGrid.LineColor = System.Drawing.Color.White;
            chartArea1.AxisY.Maximum = 3D;
            chartArea1.AxisY.Minimum = -3D;
            chartArea1.BackColor = System.Drawing.Color.Black;
            chartArea1.BorderColor = System.Drawing.Color.White;
            chartArea1.Name = "ChartArea1";
            chartArea1.ShadowColor = System.Drawing.Color.White;
            this.chart2.ChartAreas.Add(chartArea1);
            this.chart2.Location = new System.Drawing.Point(560, 308);
            this.chart2.Name = "chart2";
            this.chart2.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.Fire;
            series1.BorderWidth = 2;
            series1.ChartArea = "ChartArea1";
            series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            series1.Name = "Series1";
            this.chart2.Series.Add(series1);
            this.chart2.Size = new System.Drawing.Size(391, 321);
            this.chart2.TabIndex = 4;
            this.chart2.Text = "chart1";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1218, 700);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.Head);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Form1";
            this.Text = "Form1";
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.Form1_Paint);
            this.Head.ResumeLayout(false);
            this.Head.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Button_Close)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bunifuImageButton1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Button_minimaze)).EndInit();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ris_main)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Bunifu.Framework.UI.BunifuElipse bunifuElipse1;
        private System.Windows.Forms.Panel Head;
        private System.Windows.Forms.Label label1;
        private Bunifu.Framework.UI.BunifuImageButton Button_Close;
        private Bunifu.Framework.UI.BunifuImageButton Button_minimaze;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private Bunifu.Framework.UI.BunifuDragControl bunifuDragControl1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox text_m2;
        private System.Windows.Forms.TextBox text_f2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox text_m1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        private Bunifu.Framework.UI.BunifuFlatButton Button_Run;
        private System.Windows.Forms.PictureBox ris_main;
        private Bunifu.Framework.UI.BunifuFlatButton Button_Stop;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Timer timer;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
        private Bunifu.Framework.UI.BunifuSlider Slider_2;
        private Bunifu.Framework.UI.BunifuSlider Slider_1;
        private System.Windows.Forms.TextBox text_f1;
        private Bunifu.Framework.UI.BunifuImageButton bunifuImageButton1;
        private System.Windows.Forms.TextBox text_h;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart2;

    }
}

