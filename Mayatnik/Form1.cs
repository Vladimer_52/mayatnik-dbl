﻿


using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing.Drawing2D;



namespace Mayatnik
{
    public partial class Form1 : Form
    {

        Pen Pen_osi, Pen_nit;
        

        Brush Brush_circle, Brush_white, Brush_Black;
        Bitmap bmp;

        const double g = 9.8;

        double m1 = 2, m2 = 6;
        double l1 = 100, l2 = 100, l = 100;
   
        double x1, x2, y1, y2;
        double h = 0.1; // шаг в РК4

        double M;

        struct Z
        {
            public double a1 ;
            public double a2;
            public double p1;
            public double p2;
        }

        Z vectorZ; // вектор параметров
       
    
             Z vectorY1;
             Z vectorY2;
             Z vectorY3;
            Z vectorY4;
       
 

        public Form1()
        {
            InitializeComponent();

            Pen_osi = new Pen(Color.Black, 1);
            Pen_nit = new Pen(Color.Black, 3);
            Brush_circle = Brushes.Red;
            Brush_white = Brushes.White;
            Brush_Black = Brushes.Black ;

            vectorZ.a1 = 60 * Math.PI / 180;
            vectorZ.a2 = 30 * Math.PI / 180;
            vectorZ.p1 = 0;
            vectorZ.p2 = 0;

             Coord(); // пересчет координат
          
            
                 DrawGraf_animate(ris_main.Size);
            
            M = m2 / m1;
        }


        private void Coord()
        {
            x1 = -l1 * Math.Sin((vectorZ.a1 + Math.PI) /** Math.PI / 180*/);
            x2 = -l1 * Math.Sin((vectorZ.a1) + Math.PI) - l2 * Math.Sin((vectorZ.a2) + Math.PI);
            y1 = -l1 * Math.Cos((vectorZ.a1) + Math.PI);
            y2 = -l1 * Math.Cos((vectorZ.a1) + Math.PI) - l2 * Math.Cos((vectorZ.a2) + Math.PI);
      
        }

        private void Button_Close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Button_Run_Click(object sender, EventArgs e)
        {
            h = Convert.ToDouble(text_h.Text);
            m1 = Convert.ToDouble(text_m1.Text);
            m2 = Convert.ToDouble(text_m2.Text);
            Button_Stop.Enabled = true;
            Button_Run.Enabled = false;

            timer.Enabled = true;

      


        }

        private void Button_Stop_Click(object sender, EventArgs e)
        {
            Button_Stop.Enabled = false;
            Button_Run.Enabled = true;
           
            timer.Enabled = false;

            vectorZ.a1 = Slider_1.Value * Math.PI / 180;
            vectorZ.a2 = Slider_2.Value * Math.PI / 180;
            Coord();
            DrawGraf_main(ris_main.Size);

            chart1.Series[0].Points.Clear();
            chart2.Series[0].Points.Clear();

        }

        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            
                   chart1.Series[0].Points.Clear();
            DrawGraf_main(ris_main.Size);
        }



        //============================================================//
        //                   Метод Рунге-Кутты-4                      // 
        //============================================================//
        #region RK4
        double F1(double a1,double a2,double p1,double p2)
        {
            return  (p1*l2 - p2*l1 * Math.Cos(a1 - a2)) / (l1*l1*l2* (m1 + m2 * (Math.Sin(a1 - a2) * Math.Sin(a1 - a2))));
        }

        double F2(double a1, double a2, double p1, double p2)
        {
            return  (p2 * (m1 + m2)* l1 - p1 * m2 * l2 * Math.Cos(a1 - a2)) / (m2 * l1 * l2 *l2 * (m1 + m2 * (Math.Sin(a1 - a2) * Math.Sin(a1 - a2))));
        }

        double F3(double a1, double a2, double p1, double p2)
        {
            double A1 = ( p1 *  p2 * Math.Sin( a1 -  a2)) / ( l1 * l2 * (m1 + m2 * (Math.Sin( a1 -  a2) * Math.Sin( a1 -  a2))));

            double A2 = ((p1 * p1*m2 * l2 * l2 - 2* p1 * p2 * m2 * l1 * l2 * Math.Cos( a1 -  a2) + p2 * p2 * (m1 + m2) * l1 * l1) * Math.Sin(2 * (a1 -  a2)))
                        /(  2 * l1 * l1 * l2 * l2 * (m1 + m2 * (Math.Sin( a1 -  a2)*Math.Sin( a1 -  a2)))*(m1 + m2 * (Math.Sin( a1 -  a2)*Math.Sin( a1 -  a2)))  );

            return  -(m1+m2) * g * l1 * Math.Sin(a1) - A1 + A2;
        }

        double F4(double a1, double a2, double p1, double p2)
        {
            double A1 = (p1 * p2 * Math.Sin(a1 - a2)) / (l1 * l2 * (m1 + m2 * (Math.Sin(a1 - a2) * Math.Sin(a1 - a2))));

            double A2 = ((p1 * p1 * m2 * l2 * l2 - 2 * p1 * p2 * m2 * l1 * l2 * Math.Cos(a1 - a2) + p2 * p2 * (m1 + m2) * l1 * l1) * Math.Sin(2 * (a1 - a2)))
                        / (2 * l1 * l1 * l2 * l2 * (m1 + m2 * (Math.Sin(a1 - a2) * Math.Sin(a1 - a2))) * (m1 + m2 * (Math.Sin(a1 - a2) * Math.Sin(a1 - a2))));

            return  -m2 * M * g * l2 * Math.Sin( a2) + A1 - A2;
        }


        /*
         *   double F1(double a1,double a2,double p1,double p2)
        {
            return  (p1 - p2 * Math.Cos(a1 - a2)) / (m1 * l * l * (1 + M * Math.Sin(a1 - a2) * Math.Sin(a1 - a2)));
        }

        double F2(double a1, double a2, double p1, double p2)
        {
            return  (p2 * (1 + M) - p1 * M * Math.Cos(a1 - a2)) / (m1 * l * l * (1 + M * Math.Sin(a1 - a2) * Math.Sin(a1 - a2)));
        }

        double F3(double a1, double a2, double p1, double p2)
        {
            double A1 = ( p1 *  p2 * Math.Sin( a1 -  a2)) / (m1 * l * l * (1 + M * (Math.Sin( a1 -  a2) * Math.Sin( a1 -  a2))));

            double A2 = (( p1 * M - 2 *  p1 *  p2 * M * Math.Cos( a1 -  a2) +  p2 *  p2 * (1 + M)) +  p2 *  p2 * (1 + M) * Math.Sin(2 * ( a1 -  a2)))
                        / (2 * m1 * l * l * ((1 + M * (Math.Sin( a1 -  a2) * Math.Sin( a1 -  a2))) * (1 + M * (Math.Sin( a1 -  a2) * Math.Sin( a1 -  a2)))));

            return  -m1 * (1 + M) * g * l * Math.Sin( a1) - A1 + A2;
        }

        double F4(double a1, double a2, double p1, double p2)
        {
            double A1 = ( p1 *  p2 * Math.Sin( a1 -  a2)) / (m1 * l * l * (1 + M * (Math.Sin( a1 -  a2) * Math.Sin( a1 -  a2))));

            double A2 = (( p1 * M - 2 *  p1 *  p2 * M * Math.Cos( a1 -  a2) +  p2 *  p2 * (1 + M)) +  p2 *  p2 * (1 + M) * Math.Sin(2 * ( a1 -  a2)))
                        / (2 * m1 * l * l * ((1 + M * (Math.Sin( a1 -  a2) * Math.Sin( a1 -  a2))) * (1 + M * (Math.Sin( a1 -  a2) * Math.Sin( a1 -  a2)))));

            return  -m1 * M * g * l * Math.Sin( a2) + A1 - A2;
        }
         * 
         * */

        private void RKY(Z vectorz)
        {

             vectorY1.a1 = h * F1(vectorz.a1, vectorz.a2, vectorz.p1, vectorz.p2);
             vectorY1.a2 = h * F2(vectorz.a1, vectorz.a2, vectorz.p1, vectorz.p2);
             vectorY1.p1 = h * F3(vectorz.a1, vectorz.a2, vectorz.p1, vectorz.p2);
             vectorY1.p2 = h * F4(vectorz.a1, vectorz.a2, vectorz.p1, vectorz.p2);

             vectorY2.a1 = h * F1(vectorz.a1 + h * 0.5 *  vectorY1.a1, vectorz.a2 + h * 0.5 *  vectorY1.a2, vectorz.p1 + h * 0.5 *  vectorY1.p1, vectorz.p2 + h * 0.5 *  vectorY1.p2);
             vectorY2.a2 = h * F2(vectorz.a1 + h * 0.5 *  vectorY1.a1, vectorz.a2 + h * 0.5 *  vectorY1.a2, vectorz.p1 + h * 0.5 *  vectorY1.p1, vectorz.p2 + h * 0.5 *  vectorY1.p2);
             vectorY2.p1 = h * F3(vectorz.a1 + h * 0.5 *  vectorY1.a1, vectorz.a2 + h * 0.5 *  vectorY1.a2, vectorz.p1 + h * 0.5 *  vectorY1.p1, vectorz.p2 + h * 0.5 *  vectorY1.p2);
             vectorY2.p2 = h * F4(vectorz.a1 + h * 0.5 *  vectorY1.a1, vectorz.a2 + h * 0.5 *  vectorY1.a2, vectorz.p1 + h * 0.5 *  vectorY1.p1, vectorz.p2 + h * 0.5 *  vectorY1.p2);

             vectorY3.a1 = h * F1(vectorz.a1 + h * 0.5 *  vectorY2.a1, vectorz.a2 + h * 0.5 *  vectorY2.a2, vectorz.p1 + h * 0.5 *  vectorY2.p1, vectorz.p2 + h * 0.5 *  vectorY2.p2);
             vectorY3.a2 = h * F2(vectorz.a1 + h * 0.5 *  vectorY2.a1, vectorz.a2 + h * 0.5 *  vectorY2.a2, vectorz.p1 + h * 0.5 *  vectorY2.p1, vectorz.p2 + h * 0.5 *  vectorY2.p2);
             vectorY3.p1 = h * F3(vectorz.a1 + h * 0.5 *  vectorY2.a1, vectorz.a2 + h * 0.5 *  vectorY2.a2, vectorz.p1 + h * 0.5 *  vectorY2.p1, vectorz.p2 + h * 0.5 *  vectorY2.p2);
             vectorY3.p2 = h * F4(vectorz.a1 + h * 0.5 *  vectorY2.a1, vectorz.a2 + h * 0.5 *  vectorY2.a2, vectorz.p1 + h * 0.5 *  vectorY2.p1, vectorz.p2 + h * 0.5 *  vectorY2.p2);

             vectorY4.a1 = h * F1(vectorz.a1 + h *  vectorY3.a1, vectorz.a2 + h *  vectorY3.a2, vectorz.p1 + h *  vectorY3.p1, vectorz.p2 + h *  vectorY3.p2);
             vectorY4.a2 = h * F2(vectorz.a1 + h *  vectorY3.a1, vectorz.a2 + h *  vectorY3.a2, vectorz.p1 + h *  vectorY3.p1, vectorz.p2 + h *  vectorY3.p2);
             vectorY4.p1 = h * F3(vectorz.a1 + h *  vectorY3.a1, vectorz.a2 + h *  vectorY3.a2, vectorz.p1 + h *  vectorY3.p1, vectorz.p2 + h *  vectorY3.p2);
             vectorY4.p2 = h * F4(vectorz.a1 + h *  vectorY3.a1, vectorz.a2 + h *  vectorY3.a2, vectorz.p1 + h *  vectorY3.p1, vectorz.p2 + h *  vectorY3.p2);
        }

        #endregion
        //============================================================//

        private void timer_Tick(object sender, EventArgs e)
        {
            
            
            RKY(vectorZ);


            vectorZ.a1 +=  Math.Round(h / 6.0 * (vectorY1.a1 + 2 * vectorY2.a1 + 2 * vectorY3.a1 + vectorY4.a1), 3);
            vectorZ.a2 +=  Math.Round(h / 6.0 * ( vectorY1.a2 + 2 *  vectorY2.a2 + 2 *  vectorY3.a2 +  vectorY4.a2), 3);
            vectorZ.p1 +=  Math.Round(h / 6.0 * ( vectorY1.p1 + 2 *  vectorY2.p1 + 2 *  vectorY3.p1 +  vectorY4.p1), 3);
            vectorZ.p2 +=  Math.Round(h / 6.0 * (vectorY1.p2 + 2 * vectorY2.p2 + 2 * vectorY3.p2 + vectorY4.p2), 3);

           

            Coord(); // пересчет координат

            chart1.Series[0].Points.AddXY(x2, -y2);

            chart2.Series[0].Points.AddXY(vectorZ.a2, F2(vectorZ.a1, vectorZ.a2, vectorZ.p1, vectorZ.p2));
           
                DrawGraf_animate(ris_main.Size);
           
        }

      
        //============================================================//




        //============================================================//
        //             функция для рисования графиков                 // 
        //============================================================//


        #region Graph
        private void DrawGraf_main(Size sz)
        {
            int y = sz.Height/2;
            int x = sz.Width / 2;

            Pen pen = new Pen(Color.LightGray, 1);
            pen.DashStyle = System.Drawing.Drawing2D.DashStyle.Dash;         

             bmp = new Bitmap( ris_main.ClientSize.Width,  ris_main.ClientSize.Height);
             using (Graphics g = Graphics.FromImage(bmp))
             {

                 g.SmoothingMode = SmoothingMode.HighQuality;
                 g.FillRectangle(Brush_white, 0, 0, sz.Width, sz.Height);
                /*----------------------------------------*/
                 g.DrawLine(pen, x, 0, x, sz.Height);
                 g.DrawLine(pen, 0, y, sz.Width, y);

                 g.DrawLine(pen, x + Convert.ToSingle(x1), y + Convert.ToSingle(y1), x + Convert.ToSingle(x1), y);
                 g.DrawLine(pen, x + Convert.ToSingle(x2), y + Convert.ToSingle(y2), x + Convert.ToSingle(x2), y);

                 g.DrawLine(pen, x, y + Convert.ToSingle(y1), x + Convert.ToSingle(x1), y + Convert.ToSingle(y1));
                 g.DrawLine(pen, x, y + Convert.ToSingle(y2)+10, x + Convert.ToSingle(x2), y + Convert.ToSingle(y2)+10);
                 /*---------------------------------------*/

                 g.FillEllipse(Brush_Black, x - 5, y - 5, 10, 10);
                 g.DrawLine(Pen_nit, x, y, x + Convert.ToSingle(x1),Convert.ToSingle(y1) + y);         
                 g.DrawLine(Pen_nit, x+ Convert.ToSingle(x1), Convert.ToSingle(y1) + y, x + Convert.ToSingle(x2), Convert.ToSingle(y2) + 10 + y);

                 g.FillEllipse(Brush_circle, x + Convert.ToSingle(x1) - 10, Convert.ToSingle(y1) + y - 10, 20, 20);
                 g.FillEllipse(Brush_circle, x + Convert.ToSingle(x2) -10, Convert.ToSingle(y2) + y, 20, 20);
                 
             }
             ris_main.Image = bmp;

           
        }



        private void DrawGraf_animate(Size sz)
        {
            int y = sz.Height / 2;
            int x = sz.Width / 2;

            Pen pen = new Pen(Color.LightGray, 1);
            pen.DashStyle = System.Drawing.Drawing2D.DashStyle.Dash;

            bmp = new Bitmap(ris_main.ClientSize.Width, ris_main.ClientSize.Height);
            using (Graphics g = Graphics.FromImage(bmp))
            {

                g.SmoothingMode = SmoothingMode.HighQuality;
                g.FillRectangle(Brush_white, 0, 0, sz.Width, sz.Height);
                /*----------------------------------------*/
                g.DrawLine(pen, x, 0, x, sz.Height);
                g.DrawLine(pen, 0, y, sz.Width, y);       
                /*---------------------------------------*/

                g.FillEllipse(Brush_Black, x - 5, y - 5, 10, 10);
                g.DrawLine(Pen_nit, x, y, x + Convert.ToSingle(x1), Convert.ToSingle(y1) + y);
                g.DrawLine(Pen_nit, x + Convert.ToSingle(x1), Convert.ToSingle(y1) + y, x + Convert.ToSingle(x2), Convert.ToSingle(y2) + 10 + y);

                g.FillEllipse(Brush_circle, x + Convert.ToSingle(x1) - 10, Convert.ToSingle(y1) + y - 10, 20, 20);
                g.FillEllipse(Brush_circle, x + Convert.ToSingle(x2) - 10, Convert.ToSingle(y2) + y, 20, 20);

            }
            ris_main.Image = bmp;


        }

        #endregion

        //============================================================//
        private void Slider_1_ValueChanged(object sender, EventArgs e)
        {
            text_f1.Text = Convert.ToString(Slider_1.Value); 
            vectorZ.a1 = Slider_1.Value * Math.PI / 180;
            Coord();
            DrawGraf_main(ris_main.Size);
        }

        private void Slider_2_ValueChanged(object sender, EventArgs e)
        {
            text_f2.Text = Convert.ToString(Slider_2.Value);
            vectorZ.a2 = Slider_2.Value * Math.PI / 180;
            Coord();
            DrawGraf_main(ris_main.Size);

            
        }

    

        private void text_f1_TextChanged(object sender, EventArgs e)
        {
            Slider_1.Value = Convert.ToInt16(text_f1.Text);

        }

        private void text_f2_TextChanged(object sender, EventArgs e)
        {
            Slider_2.Value = Convert.ToInt16(text_f2.Text);
        }

        private void Button_Reboot_Click(object sender, EventArgs e)
        {
            Application.Restart();
        }

        private void Button_minimaze_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

      




      

     
       
    }
}
